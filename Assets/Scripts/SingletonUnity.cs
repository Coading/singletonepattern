using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingletonUnity : MonoBehaviour
    {
        private static SingletonUnity instance = null;

        public static SingletonUnity Instance
        {
            get
            {
                if (instance == null)
                {
                    SingletonUnity[] allsingletonsInScene = 
                        GameObject.FindObjectsOfType<SingletonUnity>();

                    if (allsingletonsInScene != null && allsingletonsInScene.Length > 0)
                    {
                        if (allsingletonsInScene.Length > 1)
                        {
                            Debug.LogWarning("Already Define SingleTone in the scene!");

                            for (int i = 1; i < allsingletonsInScene.Length; i++)
                            {
                                Destroy(allsingletonsInScene[i].gameObject);
                            }
                        }

                        instance = allsingletonsInScene[0];

                        // �ʱ�ȭ
                        instance.FakeConstructor();
                    }
                    else
                    {
                        Debug.LogError($"Not Define SingTone Object");
                    }
                }

                return instance;
            }
        }


        private float randomNumber;

        private void FakeConstructor()
        {
            randomNumber = Random.Range(0f, 1f);
        }

        public void TestSingleton()
        {
            Debug.Log($"Hello Singleton, random number is : {randomNumber}");
        }
    }
}
