using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class GameController : MonoBehaviour
    {
        // Git�׽�Ʈ 
        public void TestCharpSigleton()
        {
            Debug.Log("C#");

            SingletonCSharp instance = SingletonCSharp.Instance;
            instance.TestSingleton();

            SingletonCSharp instanc2 = SingletonCSharp.Instance;
            instanc2.TestSingleton();
        }

        public void TestUnitySingleton()
        {            
            Debug.Log("Unity");

            SingletonUnity instance = SingletonUnity.Instance;
            instance.TestSingleton();

            SingletonUnity instance2 = SingletonUnity.Instance;
            instance2.TestSingleton();
        }
    }
}
